$(document).ready(function() {
  $('#slides').superslides({
  	play: 4000,
    slide_easing: 'easeInOutCubic',
    //slide_speed: 2000,
    pagination: true,
    hashchange: false,
    scrollable: false
  });
});
